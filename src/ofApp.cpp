#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(255);
    ofEnableAlphaBlending();
    ofBlendMode(OF_BLENDMODE_ADD);
    displayHud = true;
    
    lj.setup(2000, 4,8,16,3,6,12);
    
    isSavingPDF = false;
    
    gui.setup();
    gui.add(pointCount.setup("pointCount", 1000,3,3000));
    gui.add(connectionDistance.setup("connectionDistance", 100,1,1000));
    gui.add(freqX.setup("freqX", 1,0,100));
    gui.add(freqY.setup("freqY", 1,0,100));
    gui.add(freqZ.setup("freqZ", 0,0,100));
    gui.add(modX.setup("modX", 0,0,100));
    gui.add(modY.setup("modY", 0,0,100));
    gui.add(modZ.setup("modZ", 0,0,100));
    gui.add(bgColor.setup("bgColor", ofColor(90,90,90),ofColor(0,0,0),ofColor(220,220,220)));
    gui.add(lineColor.setup("lineColor", ofColor(255,255,255,255),ofColor(0,0,0,0),ofColor(255,255,255,100)));
    gui.add(meshColorInner.setup("meshColorInner", ofColor(255,255,255,255),ofColor(0,0,0,0),ofColor(255,255,255,100)));
    gui.add(meshColorOuter.setup("meshColorOuter", ofColor(255,255,255,255),ofColor(0,0,0,0),ofColor(255,255,255,100)));
}

//--------------------------------------------------------------
void ofApp::update(){
    lj.update();
    lj.freqX = freqX;
    lj.freqY = freqY;
    lj.freqZ = freqZ;
    lj.modFreqX = modX;
    lj.modFreqY = modY;
    lj.modFreqZ = modZ;
    lj.pointCount = pointCount;
    lj.lineColor = lineColor;
    lj.colorA = meshColorInner;
    lj.colorB = meshColorOuter;
    lj.colorC = meshColorOuter;
    lj.connectionDistance = connectionDistance;
    ofBackground(bgColor);
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    if (displayHud)
    {
        gui.draw();
    }
    if (isSavingPDF)
    {
        //cout << "save!" << endl;
        ofBeginSaveScreenAsPDF("screenshot-"+ofGetTimestampString()+".pdf", false);
    }
    lj.draw();
    if (isSavingPDF)
    {
        cout << "save!" << endl;
        ofEndSaveScreenAsPDF();
        isSavingPDF = false;
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key=='s')
    {
       // isSavingPDF = true;
        ofImage screenShot;
        screenShot.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
        screenShot.save("screenshot-"+ofGetTimestampString()+".png");
    }
    if (key == 'h')
    {
        displayHud = !displayHud;
    }
    if (key == 'w')
    {
        lj.drawWeb = !lj.drawWeb;
    }
    if (key == 'm')
    {
        lj.drawMesh = !lj.drawMesh;
    }
    if (key == 'l')
    {
        lj.drawLines = !lj.drawLines;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
