//
//  Lissajous.cpp
//  LissajousFigures
//
//  Created by Ryan Cashman on 6/18/16.
//
//

#include "Lissajous.h"

Lissajous::Lissajous()
{
    
}

void Lissajous::setup(float pointCount, float freqX, float freqY, float freqZ, float modX, float modY, float modZ)
{
    this->pointCount = pointCount;
    this->freqX = freqX;
    this->freqY = freqY;
    this->freqZ = freqZ;
    modFreqX = modX;
    modFreqY = modY;
    modFreqZ = modZ;
    colorA = ofColor(1,1,155,25);
    colorB = ofColor(2,255,255,65);
    colorC = ofColor(2,255,255,65);
    lineColor = ofColor(111,111,111,77);
    
    phi = 0;
    mesh.setMode(OF_PRIMITIVE_TRIANGLE_FAN);
    drawLines = true;
    drawMesh = true;
    drawWeb = true;
}

void Lissajous::update()
{
    phi = ofGetElapsedTimef()*0.01;
    
    //if(drawLines)
    //{
        updateLine();
    //}
    
    if (drawMesh)
    {
        updateMesh();
    }
    
    if (drawWeb)
    {
        updateWeb();
    }
    
}

void Lissajous::updateLine()
{
    polyline.clear();
    float scaleFactor = ofGetHeight()/3;
    for (int i = 0; i <= pointCount; i++)
    {
        float angle = ofMap(i, 0, pointCount, 0, TWO_PI);
        float x  = (sin(angle*freqX+ofRadToDeg(phi)) * cos(angle*modFreqX)) * scaleFactor;
        float y = (sin(angle*freqY) * cos(angle*modFreqY)) * scaleFactor;
        float z = (sin(angle*freqZ * cos(angle*modFreqZ))) * scaleFactor;
        polyline.addVertex(x,y,z);
        if (i==0)
        {
            polyline.addVertex(x,y,z);
        }
    }
}

void Lissajous::updateMesh()
{
    mesh.clear();
    vector <ofPoint> verts = polyline.getVertices();
    for (int m = 0; m<pointCount-2;m++)
    {
        if (m%3==0)
        {
            mesh.addVertex(ofPoint(0,0,0));
            mesh.addColor(colorA);
            mesh.addVertex(verts[m]);
            mesh.addColor(colorB);
            mesh.addVertex(verts[m+2]);
            mesh.addColor(colorC);
        }
    }
}

void Lissajous::updateWeb()
{
    vector <ofPoint> verts = polyline.getVertices();
    web.clear();
    webAlpha.clear();
    float connectionRadius = connectionDistance;
    for (int i = 0; i < pointCount; i++)
    {
        for (int j = 0; j < i; j++)
        {
            ofPoint p1 = verts[i];
            ofPoint p2 = verts[j];
            float d = ofDist(p1.x, p1.y, p1.z, p2.x, p2.y, p2.z);
            float a = pow(1/(d/connectionRadius+1), 6);
            
            if (d <= connectionRadius)
            {
                ofPolyline segment;
                segment.addVertex(p1);
                segment.addVertex(p2);
                web.push_back(segment);
                webAlpha.push_back(a);
            }
        }
        
    }
}

void Lissajous::draw()
{
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    ofNoFill();
    ofSetColor(lineColor);
    if (drawLines)
    {
        polyline.draw();
    }
    if (drawMesh)
    {
        mesh.draw();
    }
    if (drawWeb)
    {
        for (int i = 0; i < web.size(); i++)
        {
            ofSetColor(lineColor.r, lineColor.g, lineColor.b, lineColor.a*webAlpha[i]);
            web[i].draw();
        }
    }
}