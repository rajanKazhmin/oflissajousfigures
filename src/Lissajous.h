//
//  Lissajous.h
//  LissajousFigures
//
//  Created by Ryan Cashman on 6/18/16.
//
//

#ifndef Lissajous_h
#define Lissajous_h

#include "ofMain.h"

class Lissajous
{
public:
    void setup(float pointCount, float freqX, float freqY, float freqZ, float modX, float modY, float modZ);
    void update();
    void draw();
    
    int pointCount;
    float connectionDistance;
    float freqX;
    float freqY;
    float freqZ;
    float modFreqX;
    float modFreqY;
    float modFreqZ;
    float phi;
    
    bool drawLines;
    bool drawMesh;
    bool drawWeb;
    
    ofColor colorA;
    ofColor colorB;
    ofColor colorC;
    ofColor lineColor;
    
    ofPolyline polyline;
    vector <ofPolyline> web;
    vector <float> webAlpha;
    ofMesh mesh;
    
    Lissajous();
    
    void updateLine();
    void updateMesh();
    void updateWeb();
    
private:
    
};

#endif /* Lissajous_h */
